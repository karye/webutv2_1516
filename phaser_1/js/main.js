var game = new Phaser.Game(800, 600, Phaser.AUTO, '', { preload: preload, create: create, update: update });
var platforms, ground, ledge, player, stars, cursors;
var score = 0, scoreText;

function preload() {
    game.load.image('sky', 'assets/sky2.jpg');
    game.load.image('ground', 'assets/platform.png');
    game.load.image('star', 'assets/star.png');
    game.load.spritesheet('dude', 'assets/frog.png', 79, 60, 12);
}

function create() {
    game.physics.startSystem(Phaser.Physics.ARCADE);

    // Infogar bakgrund
    game.add.sprite(0, 0, 'sky');

    // Vi skapar en grupp för alla platformar
    platforms = game.add.group();

    // Platformen får en kropp
    platforms.enableBody = true;

    // Vi skapar marken och anpassar bredden
    ground = platforms.create(0, game.world.height - 64, 'ground');
    ground.scale.setTo(2, 2);
    ground.body.immovable = true;

    // Vi skapa platformarna
    ledge = platforms.create(400, 400, 'ground');
    ledge.body.immovable = true;
    ledge = platforms.create(-150, 250, 'ground');
    ledge.body.immovable = true;
    ledge = platforms.create(550, 160, 'ground');
    ledge.body.immovable = true;

    // Figuren
    player = game.add.sprite(32, game.world.height - 250, 'dude');
    player.anchor.setTo(0.5, 0.5);

    // Ge figuren kropp och tyngd mm
    game.physics.arcade.enable(player);
    player.body.gravity.y = 300;
    player.body.bounce.y = 0.2;
    player.body.collideWorldBounds = true;

    // Animera figuren mha dess sprite-rutor
    player.animations.add('left', [0, 1, 2], 10, true);
    player.animations.add('right', [3, 4, 5], 10, true);

    // Lyssna på tangentbordet
    cursors = game.input.keyboard.createCursorKeys();

    // Vi skapar en massa stjärnor
    stars = game.add.group();
    stars.enableBody = true;
    for (var i = 0; i < 12; i++) {
        // Skapa en stjärna i gruppen
        var star = stars.create(i * 70, 0, 'star');
        star.body.gravity.y = 5 + Math.random() * 5;
        star.body.bounce.y = 0.7 + Math.random() * 0.2;
    }

    // Hålla koll på poäng
    scoreText = game.add.text(16, 16, 'Score: 0', { fontSize: '32px', fill: '#000' });
}

function update() {
    // Kollision med marken
    game.physics.arcade.collide(player, platforms);

    // Nollställ horisontell hastighet
    player.body.velocity.x = 0;

    // Vad händer när vi trycker på pil-tangenterna
    if (cursors.left.isDown) {
        // Move to the left
        player.body.velocity.x = -150;
        player.animations.play('left');
    } else if (cursors.right.isDown) {
        // Move to the right
        player.body.velocity.x = 150;
        player.animations.play('right');
    } else {
        // Stand still
        player.animations.stop();
        player.frame = 6;
    }

    //  Allow the player to jump if they are touching the ground.
    if (cursors.up.isDown && player.body.touching.down) {
        player.body.velocity.y = -350;
    }

    // Kollision mellan stjärnor och platform
    game.physics.arcade.collide(stars, platforms);

    // När player träffar en stjärna
    game.physics.arcade.overlap(player, stars, collectStar, null, this);
}

function collectStar(player, star) {

    // Removes the star from the screen
    star.kill();

    //  Add and update the score
    score += 10;
    scoreText.text = 'Score: ' + score;

}
